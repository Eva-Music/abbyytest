package com.example.abbyytest;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.newandroid2hw.R;


public class CatsPicFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cats_pic, container, false);

        ImageView pic = v.findViewById(R.id.image_cat);
        if (getArguments() != null) {
            String catLink = getArguments().getString(MainActivity.LINK);
            Glide.with(this).load(catLink).into(pic);
        }
        return v;
    }
}
