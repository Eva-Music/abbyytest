package com.example.abbyytest;

import java.util.ArrayList;
import java.util.List;


public class CatInfo {

    private static List<Cat> ITEMS = new ArrayList<>();

    private static void addItem(Cat item) {
        ITEMS.add(item);
    }

    public void setCatInfo(int id, String key, String value) {
        addItem(new Cat(id, key, value));
    }

    public void removeITEMS() {
        ITEMS.removeAll(ITEMS);
    }

    public List<Cat> getItems() {
        return ITEMS;
    }

    public static class Cat {
        public final int id;
        public final String name;
        public final String link;

        public Cat(int id, String name, String link) {
            this.id = id;
            this.name = name;
            this.link = link;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
