package com.example.abbyytest;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.newandroid2hw.R;

public class MainActivity extends AppCompatActivity implements CatsFragment.OnListFragmentInteractionListener {

    public static final String LINK = "LINK";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CatsFragment catsFragment = new CatsFragment();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.container_list, catsFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onListFragmentInteraction(CatInfo.Cat item) {

        Bundle bundle = new Bundle();
        bundle.putString(LINK, item.link);
        CatsPicFragment catsPicFragment = new CatsPicFragment();
        catsPicFragment.setArguments(bundle);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_list, catsPicFragment)
                    .addToBackStack(null)
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_pictures, catsPicFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }
}
