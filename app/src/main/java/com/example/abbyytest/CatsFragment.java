package com.example.abbyytest;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.newandroid2hw.R;

public class CatsFragment extends Fragment {


    private OnListFragmentInteractionListener mListener;

    private CatInfo catInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        catInfo = new CatInfo();
        catInfo.removeITEMS();
        int length = getResources().getTextArray(R.array.CatsName).length;
        for (int i = 0; i < length - 1; i++) {
            catInfo.setCatInfo(i, getResources().getTextArray(R.array.CatsName)[i].toString(), getResources().getTextArray(R.array.CatsPic)[i].toString());
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cat_list, container, false);

        if (view instanceof RecyclerView) {
            RecyclerView recyclerView = (RecyclerView) view;

            recyclerView.addItemDecoration(new DividerItemDecoration(container.getContext(), DividerItemDecoration.VERTICAL));
            recyclerView.setAdapter(new MyCatRecyclerViewAdapter(catInfo.getItems(), mListener));
        }

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(CatInfo.Cat item);
    }
}
